using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour {

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;

	float rotationY = 0F;

	void Update ()
	{
		float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * Options.instance.mouseSensivity;
		
		rotationY += Input.GetAxis("Mouse Y") * Options.instance.mouseSensivity;
		rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
		
		transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
	}

}
