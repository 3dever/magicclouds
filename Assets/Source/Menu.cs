﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public GameObject menuLayout;

	public GameObject mainMenu;
	public GameObject playButton;
	public GameObject continueButton;
	public GameObject menuButton;
	public GameObject quitButton;

	public GameObject winText;

	public GameObject options;
	public UnityEngine.UI.Slider mouseSlider;
	public UnityEngine.UI.Slider volumeSlider;
	public UnityEngine.UI.Slider musicSlider;


	public static Menu instance;

	void Awake()
	{
		instance = this;
	}

	public void ShowMainMenu()
	{
		continueButton.SetActive(Game.playNow);
		playButton.SetActive(!Game.playNow);
		menuButton.SetActive(Game.playNow);
		quitButton.SetActive(!Game.playNow);

		options.SetActive(false);
		mainMenu.SetActive(true);

		Show ();
	}

	public void ShowOptions()
	{
		mouseSlider.value = Options.instance.mouseSensivity;
		volumeSlider.value = AudioListener.volume;
		musicSlider.value = Options.instance.musicSource.volume;

		mainMenu.SetActive(false);
		options.SetActive(true);

		Show();
	}

	public void Show()
	{
		menuLayout.SetActive(true);
	}

	public void Hide()
	{
		menuLayout.SetActive(false);
	}

	public void OnMainMenuButton()
	{
		Game.instance.FinishGame();
	}

	public void OnPlayButton() 
	{
		Game.instance.StartCoroutine( Game.instance.NewGame() );
		Hide ();
	}

	public void OnQuitButton() 
	{
		Application.Quit();
	}

}
