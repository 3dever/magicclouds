﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

	public static bool playNow;

	public static Game instance;

	// Use this for initialization
	void Awake () 
	{
		instance = this;

		playNow = false;
	}

	void Start()
	{
		Menu.instance.ShowMainMenu();
	}
	
	public IEnumerator NewGame()
	{
		Control.enable = false;

		yield return StartCoroutine( LevelGenerator.instance.Generate() );

		playNow = true;

		Control.enable = true;
	}

	public void WinGame()
	{
		Control.enable = false;

		Menu.instance.ShowMainMenu();
		Menu.instance.winText.SetActive(true);
		Menu.instance.continueButton.SetActive(false);
	}

	public void FinishGame()
	{
		Game.playNow = false;

		Application.LoadLevel(0);
	}

	
}
