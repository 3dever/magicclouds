﻿using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour {

	public AudioClip audioClip;

	public void Play()
	{
		Sounds.Play(audioClip, Control.player.position);
	}
}
