﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

	public float timer = 1;
	public string sendMessage;

	// Use this for initialization
	void Start () 
	{
		if(timer != 0) 	StartCoroutine(DestroyAfterTimer());
	}
	
	public IEnumerator DestroyAfterTimer()
	{
		yield return new WaitForSeconds(timer);
		
		if(this != null) 
		{
			if(sendMessage != "") gameObject.SendMessage(sendMessage, SendMessageOptions.DontRequireReceiver);
			else Destroy(gameObject);
		}
	}
}
