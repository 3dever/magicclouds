﻿using UnityEngine;
using System.Collections;

public class Options : MonoBehaviour {

	public float mouseSensivity;

	public AudioSource musicSource;

	AudioListener audioListenter;

	public static Options instance;

	// Use this for initialization
	void Awake () 
	{
		instance = this;

		Load ();
	}
	
	public void Load()
	{
		if(PlayerPrefs.HasKey("mouse")) mouseSensivity = PlayerPrefs.GetFloat("mouse");
		else mouseSensivity = 5;

		if(PlayerPrefs.HasKey("volume")) AudioListener.volume = PlayerPrefs.GetFloat("volume");
		else AudioListener.volume = 1;

		if(PlayerPrefs.HasKey("music")) musicSource.volume = PlayerPrefs.GetFloat("music");
		else musicSource.volume = 1;
	}

	public void Save()
	{
		PlayerPrefs.SetFloat("mouse", mouseSensivity);
		PlayerPrefs.SetFloat("volume", AudioListener.volume);
		PlayerPrefs.SetFloat("music", musicSource.volume);
	}

	public void SetMouseSensivity(float newSensivity)
	{
		mouseSensivity = newSensivity;
	}

	public void SetVolume(float volume)
	{
		AudioListener.volume = volume;
	}

	public void SetMusicVolume(float volume)
	{
		musicSource.volume = volume;
	}

}
