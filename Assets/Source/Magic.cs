﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Combination
{
	public string name;
	public string[] requiredBlocks;
	public string craftBlock;
}

public class Magic : MonoBehaviour {
	
	public GameObject selector;
	
	public AudioClip[] selectSounds;

	public Combination[] combinations;

	public static List<Block> selectedBlocks = new List<Block>();

	public static Magic instance;

	void Awake()
	{
		instance = this;
	}

	public void AddBlock(Block block)
	{
		// Do not add already added block
		if(block.selected) return;

		// Select
		selectedBlocks.Add(block);
		block.Select();

		// Check combination
		Combination currentCombination = GetCombination();
		if(selectedBlocks.Count > 0 && currentCombination != null) 
		{
			if(selectedBlocks.Count == currentCombination.requiredBlocks.Length)
			{
				MagicCombinaions.Execute(currentCombination);
			}
		}
		// Wrong combination - reset
		else
		{
			UnselectBlocks();

			// Select again (start new combination)
			selectedBlocks.Add(block);
			block.Select();
		}

		// Select sound
		if(selectedBlocks.Count > 0)
		{
			AudioClip selectSound = selectSounds[selectSounds.Length-1];
			if(selectedBlocks.Count <= selectSounds.Length) selectSound = selectSounds[selectedBlocks.Count-1];
			Sounds.Play(selectSound, block.transform.position);
		}
	}

	// Is selected blocks is part of some combination?
	public Combination GetCombination()
	{
		foreach(var combination in combinations)
		{
			bool allBlocksInCombination = true;

			List<string> requiredBlocks = new List<string>();
			foreach(string requiredBlock in combination.requiredBlocks) requiredBlocks.Add(requiredBlock);

			foreach(var block in selectedBlocks)
			{
				// Is block exists in combination list?
				bool isPartOfCombination = false;
				for(int i = 0; i < requiredBlocks.Count; i++)
				{
					string requiredBlock = requiredBlocks[i];

					// Found it!
					if(block.blockType.name == requiredBlock) 
					{
						isPartOfCombination = true;
						requiredBlocks[i] = "-1";
						break;
					}
				}

				// One of the blocks is not part of combination - lets search more
				if(!isPartOfCombination) 
				{
					allBlocksInCombination = false;
					break;
				}
			}

			// Its fine - all blocks is part of some combination
			if(allBlocksInCombination && selectedBlocks.Count <= combination.requiredBlocks.Length) return combination;
		}
		return null;
	}

	// Unselect current blocks
	public void UnselectBlocks()
	{
		foreach(var block in selectedBlocks)
		{
			if(block != null)
			{
				block.Unselect();
			}
		}

		selectedBlocks.Clear();
	}

}
