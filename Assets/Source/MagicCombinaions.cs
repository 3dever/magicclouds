﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MagicCombinaions : MonoBehaviour {

	public AudioClip flySound;
	public AudioClip exitSound;

	public static MagicCombinaions instance;

	void Awake()
	{
		instance = this;
	}

	public static void Execute(Combination combinaion)
	{
		if(combinaion.craftBlock != "") instance.StartCoroutine(instance.CraftBlock(combinaion));
		else if(combinaion.name == "Fly") instance.StartCoroutine(instance.Fly(combinaion));
		else if(combinaion.name == "Bomb") instance.StartCoroutine(instance.Bomb(combinaion));
		else if(combinaion.name == "Exit") instance.StartCoroutine(instance.Exit(combinaion));
	}

	public Block Prepare(Block targetBlock)
	{
		Control.enable = false;
		
		// Move each other blocks to target
		foreach(var block in Magic.selectedBlocks)
		{
			block.Unselect();
			
			if(block == targetBlock) continue;

			StartCoroutine( block.MoveTo(targetBlock.transform.position) );
		}

		return targetBlock;
	}

	public IEnumerator CraftBlock(Combination combination)
	{
		// Last block is target
		Block targetBlock = Magic.selectedBlocks[Magic.selectedBlocks.Count-1];
		
		Prepare(targetBlock);
		
		yield return new WaitForSeconds(0.5f);
		
		// Create a block
		LevelGenerator.instance.SpawnBlock(LevelGenerator.instance.GetBlockName(combination.craftBlock), targetBlock.transform.position);
		
		Finish();
	}

	public void Finish(Block doNotDestroy = null)
	{
		// Destroy blocks
		foreach(var block in Magic.selectedBlocks)
		{
			if(block != null && block != doNotDestroy) block.Destruct();
		}
		
		// Unselect current blocks
		Magic.instance.UnselectBlocks();
		
		// Enable control
		Control.enable = true;
	}

	
	public IEnumerator Fly(Combination combination, string targetBlockName = "Ground")
	{
		// Platform is target
		Block targetBlock = null;
		foreach(var block in Magic.selectedBlocks)
		{
			if(block.blockType.name == targetBlockName) targetBlock = block;
		}
		Prepare(targetBlock);
		
		yield return new WaitForSeconds(0.5f);
		
		// Move to the platform
		StartCoroutine( Control.instance.PlayerMoveTo( targetBlock.transform.position + new Vector3(0, 1, 0) ) );
		Sounds.Play(flySound, Control.player.position);
		
		Finish(targetBlock);
	}

	public IEnumerator Exit(Combination combination)
	{
		yield return StartCoroutine( Fly (combination, "Exit") );

		yield return new WaitForSeconds(0.5f);

		Sounds.Play(exitSound, Control.player.position);

		Game.instance.WinGame();
	}

	public IEnumerator Bomb(Combination combination)
	{
		// Last block is target
		Block targetBlock = Magic.selectedBlocks[Magic.selectedBlocks.Count-1];
		Vector3 targetPosition = targetBlock.transform.position;


		Prepare(targetBlock);
		
		yield return new WaitForSeconds(0.5f);
		
		// Create a block
		LevelGenerator.instance.SpawnBlock(LevelGenerator.instance.GetBlockName("Bomb"), targetPosition);

		targetBlock.Destruct();

		yield return new WaitForSeconds(0.5f);

		// Destroy nearests blocks
		float timer = 0.01f;
		foreach(var block in LevelGenerator.spawnedBlocks)
		{
			if(block == null || block.blockType.name == "Bomb" || block.blockType.name == "Exit") continue;

			if(Vector3.Distance(block.transform.position, targetPosition) < 2)
			{
				StartCoroutine(block.Autodestruct(timer));
				timer += 0.01f;
			}		                                 
		}
		
		Finish();
	}


}
