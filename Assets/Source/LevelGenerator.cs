﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BlockType
{
	public string name;
	public GameObject prefab;
	public int spawnChance = 0;
}

public class LevelGenerator : MonoBehaviour {
	
	public BlockType[] blockTypes;

	public static List<Block> spawnedBlocks;

	public static LevelGenerator instance;

	void Awake()
	{
		instance = this;

		spawnedBlocks = new List<Block>();
	}

	// Use this for initialization
	void Start () 
	{
		//StartCoroutine( Generate() );
	}

	public BlockType GetBlockName(string blockName)
	{
		foreach(var blockType in blockTypes)
		{
			if(blockType.name == blockName) return blockType;
		}
		return null;
	}
	
	// Generate a new level
	public IEnumerator Generate (int size = 10, int maxSymbol = 3) 
	{
		Vector3 startPosition = new Vector3(0, -size/2, 0);
		StartCoroutine( Control.instance.PlayerMoveTo( startPosition + new Vector3(0, 1, 0) ) );

		Vector3 exitPosition = new Vector3(0, size/2, 0);
		               
		yield return new WaitForEndOfFrame();

		for(int x = -size/2; x <= size/2; x++)
		{
			for(int y = -size/2; y <= size/2; y++)
			{

				for(int z = -size/2; z <= size/2; z++)
				{
					// Set position
					Vector3 position = new Vector3(x, y, z);

					// Get block type
					BlockType blockType = null;

					// Center is for platform and player spawn
					if(position == startPosition)
					{
						blockType = GetBlockName("Ground");
					}
					// Save a place at the top
					else if(position == startPosition + new Vector3(0, 1, 0))
					{
						continue;
					}
					// Set an exit
					else if(position == exitPosition)
					   {
						blockType = GetBlockName("Exit");
					}
					// Random block
					else
					{
						while(blockType == null)
						{
							blockType = blockTypes[Random.Range(0, blockTypes.Length)];
							if(blockType.spawnChance == 0 || Random.Range(1, 100) > blockType.spawnChance) blockType = null;
						}
					}

					// Keep a spaces between lines
					//if(((x & 1) == 1) || ((y & 1) == 1) || ((z & 1) == 1)) continue;

					// Do not spawn 30% of map size
					if(blockType.spawnChance > 0 && Random.Range(1, 100) < 65) continue;

					// Spawn
					SpawnBlock(blockType, position);
				}
			}
		}

	}

	// Spawn a new cube
	public void SpawnBlock(BlockType blockType, Vector3 position)
	{
		if(blockType == null)
		{
			print ("Error: block type not found"); 
			return;
		}

		GameObject newBlock = GameObject.Instantiate(blockType.prefab, position, Quaternion.identity) as GameObject;
		newBlock.transform.parent = transform;
		newBlock.GetComponent<Block>().blockType = blockType;

		spawnedBlocks.Add(newBlock.GetComponent<Block>());
	}

}
