﻿using UnityEngine;
using System.Collections;

public class Sounds : MonoBehaviour {

	public GameObject audioPrefab;

	public static Sounds instance;

	void Awake()
	{
		instance = this;
	}

	public static void Play(AudioClip audioClip, Vector3 position)
	{
		if(audioClip == null) return;

		GameObject audioInstance = GameObject.Instantiate(instance.audioPrefab, position, Quaternion.identity) as GameObject;
		AudioSource audioSource = audioInstance.GetComponent<AudioSource>();

		audioInstance.SetActive(true);

		audioSource.PlayOneShot(audioClip);
	}

}
