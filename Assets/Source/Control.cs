﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour {
	
	public MouseLook mouseLook;

	public MeshRenderer cursor;

	public static Transform player;
	public static bool enable = false;
	public static Control instance;

	// Use this for initialization
	void Awake () 
	{
		player = Camera.main.transform;

		instance = this;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Game.playNow && Input.GetKeyDown("escape"))
		{
			if(Menu.instance.menuLayout.activeSelf) 
			{
				Menu.instance.Hide ();
			}
			else Menu.instance.ShowMainMenu();
		}

		CursorControl();

		if(!Menu.instance.menuLayout.activeSelf) RaycastControl();
	}

	public static Camera GetCurrentCamera()
	{
		return Camera.main;
	}

	void CursorControl()
	{
		if(!Menu.instance.menuLayout.activeSelf)
		{
			cursor.enabled = true;

			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			mouseLook.enabled = true;
		}
		else
		{
			cursor.enabled = false;

			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
			mouseLook.enabled = false;
		}
	}

	void RaycastControl()
	{
		if(!enable) return;

		cursor.material.SetColor("_TintColor", Color.white);

		Ray ray = GetCurrentCamera().ScreenPointToRay(new Vector3(Screen.width/2, Screen.height/2, 0));
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 5))
		{
			Block block = hit.collider.GetComponent<Block>();
			if(block != null)
			{
				cursor.material.SetColor("_TintColor", Color.blue);

				if(Input.GetMouseButtonDown(0)) Magic.instance.AddBlock(block);
			}
		}
	}
	



	public IEnumerator PlayerMoveTo(Vector3 position)
	{
		while(player.position != position)
		{
			player.position = Vector3.MoveTowards(player.position, position, Time.deltaTime * 4);
			
			yield return new WaitForEndOfFrame();
		}
	}

}
