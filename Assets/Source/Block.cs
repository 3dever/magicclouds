﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

	public AudioClip spawnSound;

	[System.NonSerialized]
	public BlockType blockType;

	[System.NonSerialized]
	public bool selected;

	GameObject selectorInstance;

	void Start()
	{
		if(spawnSound != null) Sounds.Play(spawnSound, transform.position);
	}

	public void Select()
	{
		if(selected) return;
		selected = true;

		// Spawn selector
		selectorInstance =  GameObject.Instantiate(Magic.instance.selector, transform.position, Quaternion.identity) as GameObject;
		selectorInstance.transform.localScale = transform.localScale * 1.1f;
		selectorInstance.transform.parent = transform;
	}

	public void Unselect()
	{
		if(!selected) return;
		selected = false;

		// Destroy selector
		Destroy (selectorInstance);
	}

	public IEnumerator MoveTo(Vector3 position, bool destruct = false)
	{
		bool move = true;

		while(move)
		{
			yield return new WaitForEndOfFrame();

			if(this != null) transform.position = Vector3.MoveTowards(transform.position, position, Time.deltaTime * 4);

			if(this == null || transform.position == position) move = false;
		}

		if(destruct) Destruct();
	}

	public IEnumerator Autodestruct(float timer)
	{
		yield return new WaitForSeconds(timer);
		
		Destruct();
	}

	public void Destruct()
	{
		if(this == null) return;

		StopAllCoroutines();

		//LevelGenerator.spawnedBlocks.Remove(this);

		Destroy (gameObject);
	}

}
